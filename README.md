# Git configuration template

## Installation

1. Get the files

```bash
git clone git@bitbucket.org:pkoltermann/git-config.git ~/.git-config
ln -s ~/.git-config/gitconfig ~/.gitconfig
cp ~/.git-config/examples/. ~/
```

2. Customize `~/.gitconfig_private` and `~/.gitignore_global`.
3. Voila.
